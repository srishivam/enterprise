import { SLIDE_INDEX } from './actionConstants';

export const slideIndex = () => {
  return {
    type: SLIDE_INDEX,
  };
};
