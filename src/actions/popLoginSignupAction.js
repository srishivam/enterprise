import { POP_LOGIN_SIGNUP } from './actionConstants';

export const popLoginSignup = () => {
  return {
    type: POP_LOGIN_SIGNUP,
  };
};
