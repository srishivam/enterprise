import { CHANGE_PIC } from './actionConstants';

export const changePic = () => {
  return {
    type: CHANGE_PIC,
  };
};
