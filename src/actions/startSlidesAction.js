import { START_SLIDES } from './actionConstants';

export const startSlides = () => {
  return {
    type: START_SLIDES,
  };
};
