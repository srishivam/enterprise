import React, { Component } from 'react';
import TitleBar from '../components/titleBar.jsx';
import SlideShow from '../components/slideshow.jsx';
import Sales from '../components/sales.jsx';
import styles from '../stylesheets/style.module.css';

class Welcome extends React.Component {
  constructor(props) {
    super(props);
    console.log(this.props.formStyles);
  }
  render() {
    return (
      <div className={styles.container}>
        <TitleBar></TitleBar>
        <SlideShow></SlideShow>
        <Sales></Sales>
      </div>
    );
  }
}

export default Welcome;
