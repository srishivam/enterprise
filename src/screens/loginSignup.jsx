import React, { Component } from 'react';
import styles from '../stylesheets/form.module.css';
import stylesHidden from '../stylesheets/hiddenform.module.css';
import { XSquare } from 'react-feather';

class Signup extends Component {
  constructor(props) {
    super(props);
  }
  state = { showData: false };
  toggleLogin() {
    this.setState({ showData: false });
  }
  toggleSignup() {
    this.setState({ showData: true });
  }
  toggleWindow() {
    this.setState({ formStyles: stylesHidden });
  }
  getBigDivStyles() {
    return {
      display: 'flex',
      flexDirection: 'column',
      margin: 'auto',
    };
  }
  getDivStyles() {
    return {
      display: 'flex',
      flexDirection: 'row',
      margin: 'auto',
      backgroundColor: '#4a45ca',
    };
  }
  getSectionStyles() {
    return {
      display: 'flex',
      flexDirection: 'column',
    };
  }

  getLabelStyles() {
    return {
      marginTop: '20px',
    };
  }

  getInputStyles() {
    return {
      marginTop: '18px',
    };
  }
  getLastInputStyles() {
    return {
      marginTop: '16px',
    };
  }

  render() {
    let dynamicContainer;
    let loginContainer = (
      <div style={this.getBigDivStyles()}>
        <div style={this.getDivStyles()}>
          <div style={this.getSectionStyles()}>
            <label style={this.getLabelStyles()} id="username">
              Username
            </label>
            <label style={this.getLabelStyles()} id="password">
              Password
            </label>
          </div>
          <div style={this.getSectionStyles()}>
            <input style={this.getInputStyles()} type="text"></input>
            <input style={this.getInputStyles()} type="password"></input>
          </div>
        </div>
        <button>Log In</button>
      </div>
    );
    let signupContainer = (
      <div style={this.getBigDivStyles()}>
        <div style={this.getDivStyles()}>
          <div style={this.getSectionStyles()}>
            <label style={this.getLabelStyles()} id="username">
              Username
            </label>
            <label style={this.getLabelStyles()} id="password">
              Password
            </label>
            <label style={this.getLabelStyles()} id="fname">
              First Name
            </label>
            <label style={this.getLabelStyles()} id="lname">
              Last Name
            </label>
          </div>
          <div style={this.getSectionStyles()}>
            <input style={this.getInputStyles()} type="text"></input>
            <input style={this.getInputStyles()} type="password"></input>
            <input style={this.getInputStyles()} type="text"></input>
            <input style={this.getLastInputStyles()} type="text"></input>
          </div>
        </div>
        <button>Sign Up</button>
      </div>
    );

    if (this.state.showData) dynamicContainer = signupContainer;
    else dynamicContainer = loginContainer;

    return (
      <div className={styles.container}>
        <div className={styles.buttonSection}>
          <button onClick={this.toggleLogin.bind(this)}>
            Have an account?
          </button>
          <button onClick={this.toggleSignup.bind(this)}>New User?</button>
          <XSquare
            style={{ marginLeft: '10%', color: 'red', cursor: 'pointer' }}
            onClick={this.toggleWindow.bind(this)}
          ></XSquare>
        </div>
        {dynamicContainer}
      </div>
    );
  }
}

export default Signup;
