import React from '../node_modules/react';
import MainPage from './screens/mainPage';
import LoginSignup from './screens/loginSignup';
import formStyles from './stylesheets/hiddenform.module.css';
import { Provider } from 'react-redux';
import store from './store';

import './App.css';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <MainPage></MainPage>
        <LoginSignup></LoginSignup>
      </div>
    </Provider>
  );
}

export default App;
