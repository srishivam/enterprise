export { changePic } from './actions/changePicAction';
export { startSlides } from './actions/startSlidesAction';
export { slideIndex } from './actions/slideIndexAction';
export { popLoginSignup } from './actions/popLoginSignupAction';
