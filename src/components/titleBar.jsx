import React, { Component } from 'react';
import titleLogo from '../images/shoplogohead.PNG';
import searchIcon from '../images/searchicon.png';
import cartIcon from '../images/cart.png';
import styles from '../stylesheets/style.module.css';
import { Search, ShoppingCart, LogIn, HelpCircle } from 'react-feather';
import visibleStyles from '../stylesheets/form.module.css';
// const classes = {};

class TitleBar extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    cartItems: 0,
    loginName: '',
    renderLoginButton: false,
  };

  toggleWindow() {
    this.props.formStyles = visibleStyles;
  }

  render() {
    return (
      <div className={styles.header}>
        <div className={styles.logoDiv}>
          <img src={titleLogo} height="50px" />
        </div>
        <div className={styles.searchDiv}>
          <input
            className={styles.searchBox}
            type="text"
            placeholder="Search for products, brands or more"
          />
          <div className={styles.searchIcon}>
            <Search
              size="25"
              color="#db8412"
              style={{ marginTop: '5px', marginRight: '5px' }}
            ></Search>
          </div>
          <div className={styles.loginDiv}>
            <span className={styles.loginSpan}>Login</span>
            <LogIn
              size="25"
              color="#db8412"
              style={{ marginLeft: '10%', marginTop: '2px' }}
              onClick={this.toggleWindow.bind(this)}
            ></LogIn>
          </div>
          <div className={styles.cartDiv}>
            <ShoppingCart
              size="35"
              color="#ffffff"
              style={{ marginTop: '2px' }}
            ></ShoppingCart>
            <span className={styles.cartSpan}>Cart</span>
          </div>
          <HelpCircle
            size="30"
            color="#ffffff"
            style={{ marginLeft: '6%', marginTop: '2px', cursor: 'pointer' }}
          ></HelpCircle>
        </div>
      </div>
    );
  }

  checkCartItems() {}
}

export default TitleBar;
