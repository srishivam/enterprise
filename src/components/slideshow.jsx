import React, { Component } from 'react';
import styles from '../stylesheets/style.module.css';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { CSSTransitionGroup } from 'react-css-transition';
import { connect } from 'react-redux';
import { changePic, slideIndex, startSlides } from '../actionFire';
import slidePic1 from '../images/slidepic1.webp';
import slidePic2 from '../images/slidepic2.webp';
import slidePic3 from '../images/slidepic3.webp';
import slidePic4 from '../images/slidepic4.webp';
import slidePic5 from '../images/slidepic5.webp';
import rootReducer from '../rootReducer';
import store from '../store';

class SlideShow extends Component {
  state = {
    backgroundColor: 'white',
    // slideIndex: 1,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log(' slide index is ' + this.props.currentSlideIndex);
    if (!this.props.slideStatus) {
      setInterval(() => {
        //this.setState({ pic: !this.state.changePic });
        this.props.changePic();
        // console.log(this.state.changePic);
        if (this.props.picStatus == true) {
          // if (this.props.currentSlideIndex == 5) this.props.slideIndex();
          // else this.props.slideIndex();
          this.props.slideIndex();
        }
      }, 4000);
      //this.setState({ startSlides: true });
      //this.props.startSlides();
    }
  }

  render() {
    function GetFirstImage() {
      return <img src={slidePic1} width="100%" height="300px"></img>;
    }
    function GetSecondImage() {
      return <img src={slidePic2} width="100%" height="300px"></img>;
    }
    function GetThirdImage() {
      return <img src={slidePic3} width="100%" height="300px"></img>;
    }
    function GetFourthImage() {
      return <img src={slidePic4} width="100%" height="300px"></img>;
    }
    function GetFifthImage() {
      return <img src={slidePic5} width="100%" height="300px"></img>;
    }
    function GetImage(localProps) {
      switch (localProps.slideNum) {
        case 1:
          return <GetFirstImage></GetFirstImage>;
          break;
        case 2:
          return <GetSecondImage></GetSecondImage>;
          break;
        case 3:
          return <GetThirdImage></GetThirdImage>;
          break;
        case 4:
          return <GetFourthImage></GetFourthImage>;
          break;
        case 5:
          return <GetFifthImage></GetFifthImage>;
          break;
      }
    }
    return (
      <div className={styles.slideshowContainer}>
        <CSSTransition
          in={this.props.picStatus}
          timeout={4000}
          classNames={{
            enter: styles['slidePics-enter'],
            enterActive: styles['slidePics-enter-active'],
            exit: styles['slidePics-exit'],
            exitActive: styles['slidePics-exit-active'],
          }}
          unmountOnExit={true}
          mountOnEnter={true}
        >
          <GetImage slideNum={this.props.currentSlideIndex}></GetImage>
        </CSSTransition>
      </div>
    );
  }
}

// const mapStateToProps = (state) => {
//   return {
//     picStatus: state.changePic,
//     slideStatus: state.startSlides,
//     currentSlideIndex: state.slideIndex,
//   };
// };
const mapStateToProps = (state) => {
  return {
    picStatus: state.changePicReducer.changePic,
    slideStatus: state.startSlidesReducer.startSlides,
    currentSlideIndex: state.slideIndexReducer.slideIndex,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    changePic: () => dispatch(changePic()),
    startSlides: () => dispatch(startSlides()),
    slideIndex: () => dispatch(slideIndex()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SlideShow);
