import React, { Component, lazy, Suspense } from 'react';
import styles from '../stylesheets/style.module.css';
import CategoryCard from './categorycard.jsx';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import catImage1 from '../images/category1.jpeg';
import catImage2 from '../images/category2.jpeg';
import catImage3 from '../images/category3.jpeg';
import catImage4 from '../images/category4.jpeg';
import catImage5 from '../images/category5.jpeg';
import catImage6 from '../images/category6.jpeg';
import catImage7 from '../images/category7.jpeg';
import catImage8 from '../images/category8.jpeg';
import catImage9 from '../images/category9.jpeg';
import catImage10 from '../images/category10.jpeg';
import catImage11 from '../images/category11.jpeg';
import catImage12 from '../images/category12.jpeg';
import catImage13 from '../images/category13.jpeg';
import catImage14 from '../images/category14.jpeg';
import catImage15 from '../images/category15.jpeg';
import catImage16 from '../images/category16.jpeg';
import catImage17 from '../images/category17.jpeg';
import catImage18 from '../images/category18.jpeg';

class Sales extends Component {
  state = {};
  render() {
    return (
      <div className={styles.salesContainer}>
        <div className={styles.salesRow}>
          <Suspense fallback={<div>Loading........</div>}>
            <CategoryCard
              categoryTitle={'Cases & Covers'}
              imageLink={catImage1}
            ></CategoryCard>{' '}
          </Suspense>

          <CategoryCard
            categoryTitle={'Power Banks'}
            imageLink={catImage2}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Mobile Holders'}
            imageLink={catImage3}
          ></CategoryCard>
        </div>
        <div className={styles.parallax1}></div>

        <div className={styles.salesRow}>
          <CategoryCard
            categoryTitle={'Mobile Sim & SD Card Trays'}
            imageLink={catImage4}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Mobile Flashes'}
            imageLink={catImage5}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Mods'}
            imageLink={catImage6}
          ></CategoryCard>
        </div>
        <div className={styles.parallax2}></div>
        <div className={styles.salesRow}>
          <CategoryCard
            categoryTitle={'Mobile Body Panels'}
            imageLink={catImage7}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Mobile Battery'}
            imageLink={catImage8}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Screen Expander for Phones'}
            imageLink={catImage9}
          ></CategoryCard>
        </div>
        <div className={styles.parallax3}></div>
        <div className={styles.salesRow}>
          <CategoryCard
            categoryTitle={'Mobile Pouches'}
            imageLink={catImage10}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Mobile Spare Parts'}
            imageLink={catImage11}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Earphone Cable Organizers'}
            imageLink={catImage12}
          ></CategoryCard>
        </div>
        <div className={styles.parallax1}></div>

        <div className={styles.salesRow}>
          <CategoryCard
            categoryTitle={'Mobile Cables'}
            imageLink={catImage13}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Anti Radiation Stickers'}
            imageLink={catImage14}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Mobile Enhancements'}
            imageLink={catImage15}
          ></CategoryCard>
        </div>
        <div className={styles.parallax2}></div>

        <div className={styles.salesRow}>
          <CategoryCard
            categoryTitle={'Charging Pads'}
            imageLink={catImage16}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Styling & Maintainence'}
            imageLink={catImage17}
          ></CategoryCard>
          <CategoryCard
            categoryTitle={'Selfie Sticks'}
            imageLink={catImage18}
          ></CategoryCard>
        </div>
        <div className={styles.parallax3}></div>
      </div>
    );
  }
}

export default Sales;
