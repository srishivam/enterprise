import React, { Component, lazy, Suspense } from 'react';
import styles from '../stylesheets/style.module.css';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import { Eye } from 'react-feather';

class CategoryCard extends Component {
  constructor(props) {
    super(props);
  }
  state = { hover: false, loadCards: false };
  toggleHover() {
    this.setState({ hover: !this.state.hover });
  }
  componentDidMount() {
    if (!this.loadCards) this.setState({ loadCards: !this.state.loadCards });
  }
  render() {
    let imageToRender, viewMore;
    if (this.state.hover) {
      // console.log('hovered');
      imageToRender = (
        <img
          src={this.props.imageLink}
          height="250px"
          className={styles.categoryImage}
          style={{ marginTop: '0px', transition: 'margin-top 500ms' }}
        ></img>
      );
      viewMore = (
        <div className={styles.viewMore}>
          <Eye
            style={{
              display: 'flex',
              opacity: '1',
              transition: 'opacity 900ms',
              marginLeft: '30%',
              marginTop: '26px',
              color: '#db8412',
            }}
          ></Eye>
          <span
            style={{
              display: 'flex',
              opacity: '1',
              marginLeft: '5%',
              transition: 'opacity 900ms',
              marginTop: '20px',
              color: '#db8412',
            }}
          >
            <h2>View 16 products</h2>
          </span>
        </div>
      );
    } else {
      // console.log('out now');
      imageToRender = (
        <img
          src={this.props.imageLink}
          height="250px"
          className={styles.categoryImage}
          style={{ marginTop: '40px', transition: 'margin-top 500ms' }}
        ></img>
      );
      viewMore = (
        <div className={styles.viewMore}>
          <Eye
            style={{
              opacity: '0',
              transition: 'opacity 900ms',
              marginLeft: '30%',
              marginTop: '26px',
              color: '#db8412',
            }}
          ></Eye>
          <span
            style={{
              display: 'flex',
              opacity: '0',
              transition: 'opacity 900ms',
              marginLeft: '5%',
              marginTop: '20px',
              color: '#db8412',
            }}
          >
            <h2> View 16 products</h2>
          </span>
        </div>
      );
    }
    return (
      <div
        className={styles.catCard}
        onMouseEnter={this.toggleHover.bind(this)}
        onMouseLeave={this.toggleHover.bind(this)}
      >
        <h1 className={styles.catHeading}>{this.props.categoryTitle}</h1>
        <div className={styles.imageDiv}>
          <Suspense fallback={<div>Loading..</div>}> {imageToRender}</Suspense>
        </div>
        {viewMore}
      </div>
    );
  }
}

export default CategoryCard;
