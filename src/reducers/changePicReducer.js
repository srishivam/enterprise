import { CHANGE_PIC } from '../actions/actionConstants';
const initialState = { changePic: false };

const changePicReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_PIC:
      return { ...state, changePic: !state.changePic };
    default:
      return state;
  }
};

export default changePicReducer;
