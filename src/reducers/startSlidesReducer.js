import { START_SLIDES } from '../actions/actionConstants';
const initialState = { startSlides: false };

const startSlidesReducer = (state = initialState, action) => {
  switch (action.type) {
    case START_SLIDES:
      return { ...state, startSlides: !state.startSlides };
    default:
      return state;
  }
};

export default startSlidesReducer;
