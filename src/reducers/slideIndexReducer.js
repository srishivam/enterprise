import { SLIDE_INDEX } from '../actions/actionConstants';
const initialState = { slideIndex: 1 };

const slideIndexReducer = (state = initialState, action) => {
  switch (action.type) {
    case SLIDE_INDEX:
      if (state.slideIndex < 5)
        return { ...state, slideIndex: state.slideIndex + 1 };
      else return { ...state, slideIndex: 1 };
    default:
      return state;
  }
};

export default slideIndexReducer;
