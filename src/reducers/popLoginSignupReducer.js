import { POP_LOGIN_SIGNUP } from '../actions/actionConstants';
const initialState = { popLoginSignup: false };

const popLoginSignupReducer = (state = initialState, action) => {
  switch (action.type) {
    case POP_LOGIN_SIGNUP:
      return { ...state, popLoginSignup: !state.popLoginSignup };
    default:
      return state;
  }
};

export default popLoginSignupReducer;
