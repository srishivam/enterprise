import changePicReducer from './reducers/changePicReducer';
import startSlidesReducer from './reducers/startSlidesReducer';
import slideIndexReducer from './reducers/slideIndexReducer';
import popLoginSignupReducer from './reducers/popLoginSignupReducer';
import { combineReducers } from 'redux';

export default combineReducers({
  startSlidesReducer: startSlidesReducer,
  changePicReducer: changePicReducer,
  slideIndexReducer: slideIndexReducer,
  popLoginSignupReducer: popLoginSignupReducer,
});
