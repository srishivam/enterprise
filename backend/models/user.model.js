const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      minlength: 5,
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 5,
    },
    fullName: {
      type: String,
      required: true,
      minlength: 4,
    },
    emailId: {
      type: String,
      required: true,
      trim: true,
      minlength: 5,
    },
    contact: {
      type: Number,
      required: true,
      trim: true,
      minlength: 10,
    },
    gender: {
      type: String,
      required: true,
    },
    addresses: [
      {
        address: { type: String, max: 100, required: true },
        pincode: { type: String, max: 6, required: true },
        workOrHome: { type: String },
      },
    ],
    cart: [
      {
        productId: { type: Number },
        quantity: { type: Number },
      },
    ],
    wishlist: [{ productId: { type: Number } }],
    reviews: [{ productId: { type: Number }, review: { type: String } }],
    ratings: [{ productId: { type: Number }, rating: { type: Number } }],
  },
  {
    timestamps: true,
  }
);

const User = mongoose.model('User', userSchema);

module.exports = User;
