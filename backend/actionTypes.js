const redux = require('redux');
const reduxLogger = require('redux-logger');

const createStore = redux.createStore;
const combineReducers = redux.combineReducers;
const applyMiddleware = redux.applyMiddleware;
const logger = reduxLogger.createLogger();
const TOGGLE_WINDOW = 'TOGGLE_WINDOW';
const CHANGE_PIC = 'CHANGE_PIC';

function toggleWindow() {
  return {
    type: TOGGLE_WINDOW,
  };
}

function changePic() {
  return {
    type: CHANGE_PIC,
  };
}

const initialState = {
  windowState: false,
  changePic: false,
};

const initialWindowState = {
  windowState: false,
};

const initialChangePic = {
  changePic: false,
};

const windowStateReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_WINDOW:
      return { ...state, windowState: !state.windowState };
    default:
      return state;
  }
};
const changePicReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_PIC:
      return { ...state, changePic: !state.changePic };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  windowRed: windowStateReducer,
  picRed: changePicReducer,
});
const store = createStore(rootReducer, applyMiddleware(logger));
console.log('Initial state', store.getState().windowState);
const unsubscribe = store.subscribe(() => {
  console.log('Updated state', store.getState().windowRed);
});
store.dispatch(toggleWindow());
store.dispatch(toggleWindow());
store.dispatch(toggleWindow());
store.dispatch(changePic());
store.dispatch(changePic());
unsubscribe();
